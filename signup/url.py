import views
import mailtask
import welcome
urls=[('/signup',views.Signup),
	  ('/signin',views.Signin),  	
      ('/checkuser',views.Checkuser),
      ('/mailqueue',mailtask.MailTaskQueue),
      ('/send_welcome_message',welcome.WelcomeMessage),
      ('/homepage',views.Homepage),
      ('/api/getsms/?$', views.GetSms),  	
]
