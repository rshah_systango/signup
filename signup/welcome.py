import logging
import base
from google.appengine.api import mail

class WelcomeMessage(base.BaseController):
  def get(self):
    WELCOME_MESSAGE_BODY="hello you have registered on our page"
    mail_address=self.request.get("address")    
    mail.send_mail('rshah@isystango.com','vkashyap@isystango.com','Welcome Message',WELCOME_MESSAGE_BODY)
    logging.info("------------------------------------------")
