from google.appengine.ext import db
import datetime
import md5

class Members(db.Model):
  user_name=db.StringProperty()
  email=db.EmailProperty()
  address=db.PostalAddressProperty()
  password=db.StringProperty()
  date_of_birth=datetime.datetime
  
  
  @staticmethod
  def check_if_not_empty(credentials):
    if(credentials!=""):
      return True
    else:
      return False

  @staticmethod
  def check_if_user_present(user_id):
    members=db.Query(Members)
    response={}
    if(members.filter("email =",user_id).get()):
      response["already_exists"]="true"
      return response
    else:
      response["already_exists"]="false"
      return response

  @staticmethod
  def signin(user_credentials):
    members=db.Query(Members)
    signin_response={}
    if(Members.check_if_not_empty(user_credentials["user_name"]) and Members.check_if_not_empty(user_credentials["password"])):
      if(members.filter("email =",user_credentials["user_name"]).get()):
        if(members.filter("password =",user_credentials["password"]).get()):
          signin_response["user_valid"]=True
        else:
          signin_response["password_incorrect"]=True
      else:
        signin_response["user_name incorrect"]=True      
    return signin_response

  @staticmethod
  def create_member(user_details):
    response={}
    member_details_valid=True 
    member=Members()

    if(Members.check_if_not_empty(user_details["user_name"])):
      member.user_name=user_details["user_name"]
    else:
      response["user_name"]="empty"
      member_details_valid=False

    if(Members.check_if_not_empty(user_details["password"])):
      member.password=user_details["password"]
    else:
      response["password"]="empty"
      member_details_valid=False

    if(Members.check_if_not_empty(user_details["address"])):
      member.address=db.PostalAddress(user_details["address"])
    else:  
      response["address"]="empty"  
      member_details_valid=False

    if(Members.check_if_not_empty(user_details["email"])):
      member.email=db.Email(user_details["email"])
    else:
      response["email"]="empty" 
      member_details_valid=False

    if(Members.check_if_not_empty(user_details["date_of_birth"])):
      member.date_of_birth=user_details["date_of_birth"]
    else:
      response["date_of_birth"]="empty"  
      member_details_valid=False

    if(member_details_valid==True):
      member.put()
      response["status_ok"]="User Added"
      return response
    else:
      return response
        
    

