import webapp2
import views
import url

app = webapp2.WSGIApplication(url.urls, debug = True)
	
def main():
	run_wsgi_app(app)

if __name__ == '__main__':
	main()