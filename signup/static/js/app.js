var signupApp= angular.module('signupApp',[ 
'signupAppController',
'ngRoute',
'signupAppServices'
]).
config(['$routeProvider', function($routeProvider) {
  $routeProvider.when('/signup', {templateUrl: 'partials/signup.html',controller:'SignInCtrl'});
  $routeProvider.when('/login', {templateUrl: 'partials/login.html',controller:'SignInCtrl'});
  $routeProvider.when('/homepage', {templateUrl: '/homepage',controller:'SignInCtrl'});
}]);