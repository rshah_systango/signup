'use strict';
/* Services */
var signupAppServices= angular.module('signupAppServices',['ngResource']);

signupAppServices.factory('Checkid',['$resource',
	function($resource){
		return $resource('/checkuser/', {}, {
			query: {method:'GET',isArray:false}
		});
	}]);		
signupAppServices.factory('Createuser',['$resource',
	function($resource){  
 		return $resource('/signup/',{user_name:'@user_name',email:'@email',address:'@address',password:'@password',dob:'@dob'}, {
			save: {method:'POST'}
		});		
}]);

signupAppServices.factory('Signinuser',['$resource',
	function($resource){  
 		return $resource('/signin/',{user_name:'@user_name',password:'@password'}, {
			save: {method:'POST',isArray:false}
		});		
}]);
