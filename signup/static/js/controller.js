
var signupAppController= angular.module('signupAppController',[]);

signupAppController.controller('SignInCtrl',['$scope','$location','Checkid','Createuser','Signinuser',function($scope,$location,Checkid,Createuser,Signinuser){
	$scope.signinresponse="";	

	$scope.checkUser= function(){
	$scope.userPresent=angular.fromJson(Checkid.query({uid:$scope.email}))
	};

	$scope.createUser= function(){
	$scope.response = Createuser.save({user_name:$scope.user_name,email:$scope.email,address:$scope.address,password:$scope.password,dob:$scope.dob});
	};

	$scope.loadSignup =function(){
	$location.path('/signup');	
	};

	$scope.loadLogin =function(){
	$location.path('/login');	
	};

	$scope.loginUser = function(){
    $scope.signinresponse=Signinuser.save({user_name:$scope.userId,password:$scope.userPassword},function success(){
    	if($scope.signinresponse.uservalid.user_valid)
		{	
			$location.path('/homepage');
		}
	    });		
	};
}]);