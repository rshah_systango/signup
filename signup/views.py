import webapp2
import os
from google.appengine.ext.webapp import template
from google.appengine.api.labs import taskqueue
from models import Members
import base
import json
import pilvo


PLIVO_AUTH_ID = "MAODHLOTK5MJHJODIXNZ"
PLIVO_AUTH_TOKEN = "OWNmNGQ5NDMzMTQ1N2IwMzM4MjBhMTJiNDY4Nzcx"


class Signup(base.BaseController):
  def get(self):
    self.template_renderer('signup.html')

  def post(self):
    user_detais={
    "user_name":self.request.get("user_name"),
    "password":self.request.get("password"),
    "email":self.request.get("email"),
    "address":self.request.get("address"),
    "date_of_birth":self.request.get("dob")
    }

    response_message=Members.create_member(user_detais)
    if(response_message["status_ok"]):
      taskqueue.add(url='/mailqueue', params={'address':user_detais["email"]})            
      self.template_renderer('index.html',{"response":response_message})

class Checkuser(base.BaseController):
  def get(self):
    user_present=Members.check_if_user_present(self.request.get("uid"))
    self.response.write(json.dumps({"user_present_response":user_present}))

class Signin(base.BaseController):
  def get(self):
    self.template_renderer("index.html")

  def post(self):
    user_credentials={
    "user_name":self.request.get("user_name"),
    "password":self.request.get("password")
    }
    signin_response=Members.signin(user_credentials)
    if(signin_response["user_valid"]):
      self.response.write(json.dumps({"uservalid":signin_response}))
    else:  
      self.response.write({"signin_response":signin_response}) 

class Homepage(base.BaseController):
  def get(self):
    self.template_renderer("homepage.html")

class GetSms(base.BaseController):
  def get(self):
    text = self.request.get('Text')
    from_number = self.request.get('From')
    logging.info('Text received: %s - From: %s' %(text,from_number))     

  def post(self):
    src = self.request.get('src')
    dst = self.request.get('dst')
    text = self.request.get('text')
    message_params = {
      'src':src,
      'dst':dst,
      'text':text,
    }
    if src and dst and text:
      plivo_sms = plivo.RestAPI(PLIVO_AUTH_ID, PLIVO_AUTH_TOKEN) 
      response = plivo_sms.send_message(message_params)
      #self.send_response(response)

