import webapp2
import os
from google.appengine.ext.webapp import template

class BaseController(webapp2.RequestHandler):
    
    def template_renderer(self,file_name,template_values={}):
        path = self.template_location(file_name)
        self.response.out.write(template.render(path,{}))

    def template_location(self,file_name):
        return os.path.join(os.path.dirname(__file__),'template',file_name)	
    