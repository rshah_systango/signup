from google.appengine.api import mail
from google.appengine.api.labs import taskqueue
import logging
import base

class MailTaskQueue(base.BaseController):

  def post(self):
    SIGNUP_MESSAGE_BODY="hello you have registered on our page"    
    mail_address=self.request.get("address")    
    mail.send_mail('rshah@isystango.com',mail_address,'Signup Confirmation',SIGNUP_MESSAGE_BODY)
    print('Sent invitation to %s' % mail_address)
    logging.info('Sent invitation to %s' % mail_address)
